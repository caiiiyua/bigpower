//
//  BigPower.hpp
//  Bigpower
//
//  Created by 皮皮的MacBook  on 2016/10/13.
//  Copyright © 2016年 pp. All rights reserved.
//

#ifndef BigPower_hpp
#define BigPower_hpp

#include <stdio.h>
#include <iostream>

using namespace std;

#define MAX_LENGTH 600000 // 99999^99999 is about 490k digits
#define MAX_DIGITS 10000
#define MAX_DIGITS_NUM 4

class BigPower {

private:
    int n; // base
    int m; // exponentiation
    int x[MAX_LENGTH] = {0};
    int s = 1; // sections

public:
    BigPower();
    BigPower(int);
    BigPower(int, int);

    int length();
    friend ostream & operator<<(ostream & os, const BigPower & p);
};

#endif /* BigPower_hpp */
