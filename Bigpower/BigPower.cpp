//
//  BigPower.cpp
//  Bigpower
//
//  Created by 皮皮的MacBook  on 2016/10/13.
//  Copyright © 2016年 pp. All rights reserved.
//

#include "BigPower.hpp"

BigPower::BigPower() {
    n = 0;
    m = 0;
    BigPower(n, m);
};

BigPower::BigPower(int p) {
    n = p;
    m = 1;
    BigPower(n, m);
}

BigPower::BigPower(int b, int e) {
    n = b;
    m = e;
    
    int v = 0; // recording advance digital

    x[0] = 1;
    for (int a = 0; a < m; a++) {
        for (int i = 0; i < s; i++) {
            x[i] = x[i] * n + v; // calculate one section and add advanced number
            v = x[i] / MAX_DIGITS; // any numbers need to be advanced to next section
            x[i] = x[i] % MAX_DIGITS; // leave no more than 8 digits within one section
            if (v > 0 && i + 1 == s) {
                // check if one more section need to be calculated
                s++;
            }
        }
    }
}

int BigPower::length() {
    int l = 0;
    int n = x[s - 1];
    while (n > 0) {
        n = n / 10;
        l++;
    }
    
    return l + MAX_DIGITS_NUM * (s - 1);
}

ostream & operator<<(ostream & out, const BigPower & p) {
    for (int i = 1; i <= p.s; i++) {
        cout<< p.x[p.s - i];
        if (i % 20 == 0) {
            cout << endl;
        }
    }
    cout << endl;
    return out;
}
