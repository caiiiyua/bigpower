//
//  main.cpp
//  Bigpower
//
//  Created by 皮皮的MacBook  on 2016/10/13.
//  Copyright © 2016年 pp. All rights reserved.
//

#include <iostream>
#include "BigPower.hpp"

using namespace std;

#define MAX_NUM 99999

int reverseInteger(int n) {
    int num = 0;
    while (n > 0) {
        num = num * 10 + n % 10;
        n = n / 10;
    }
    return num;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Enter a number(0-99999): ";
    int n, m;
    std::cin >> n;
    if (n > MAX_NUM || n < 0) {
        cout << "Invalid number, only number within 0-99999 is available.";
        return -1;
    }
    m = reverseInteger(n);
    BigPower p = BigPower(n, reverseInteger(n));
    cout << "Result: " << n << "^" << m << " is"<< endl << p;
    cout << "Length of result is: " << p.length() << endl;
    return 0;
}
